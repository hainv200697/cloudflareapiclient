using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace CloudFlareApiClient
{
    class Program
    {      
        static void Main(string[] args)
        {
            string CF_ApiUrl = "https://api.cloudflare.com/client/v4/";
            string CF_AuthEmail = "{INSERT YOUR Cloud Flare Account Email Address HERE}";
            string CF_AuthKey = "{INSERT YOUR AUTH-KEY HERE}";
            string CF_DNS_ZONE_ID = "{INSERT YOUR CLOUD FLARE DNS ZONE ID}";
            string CF_DNS_RECORD_ID = "{INSERT YOUR CLOUD FLARE DNS RECORD ID}";
            
            string publicIpAddress = GetIPAddress(); // Get Public IP of my Home Server
      
            // Build a JSON string to PUT to CloudFlare API
            Dictionary<string, string> jsonData = new Dictionary<string, string>();
            jsonData.Add("type", "A");
            jsonData.Add("name", "mydomain.com");
            jsonData.Add("content", publicIpAddress.Trim()); // Trim Public IP Address Variable
            string jsonPostData = JsonConvert.SerializeObject(jsonData);
            
            PutJsonDataToApi(CF_ApiUrl,CF_AuthEmail,CF_AuthKey,CF_DNS_ZONE_ID,CF_DNS_RECORD_ID,jsonPostData); 
        }

        public static string GetIPAddress()
        {
            WebClient webClient = new WebClient();
            return webClient.DownloadString("https://icanhazip.com/");
        }

        public static void PutJsonDataToApi(string apiUrl, string authEmail, string authKey, string dnsZoneId, string dnsRecordId, string jsonData)
        {

            var httpWebRequest = (HttpWebRequest)WebRequest.Create(apiUrl+ "/zones/" + dnsZoneId + "/dns_records/" + dnsRecordId);
            httpWebRequest.ReadWriteTimeout = 100000; 
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Accept = "*/*";
            httpWebRequest.Method = "PUT"; 
            httpWebRequest.Headers.Add("X-Auth-Email", authEmail);
            httpWebRequest.Headers.Add("X-Auth-Key", authKey);

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                jsonData = jsonData.Replace("\n", "");
                jsonData = jsonData.Replace("\r", "");
                streamWriter.Write(jsonData);
                streamWriter.Flush();
                streamWriter.Close();
            }

            try
            {
                HttpWebResponse resp = (HttpWebResponse)httpWebRequest.GetResponse();
                string respStr = new StreamReader(resp.GetResponseStream()).ReadToEnd();
                Console.WriteLine("Response : " + respStr); // Output response to Console Window
                Console.ReadLine(); 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();  
            }
        }
    }
}
